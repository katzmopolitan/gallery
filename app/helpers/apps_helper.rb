module AppsHelper
  def software_photo_url(software)
    software.url || gallery_fallback_photo
  end

  protected

  def gallery_fallback_photo
    image_path('home/submission_fallback_image.gif')
  end
end
