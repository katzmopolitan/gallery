require 'sidekiq/web'

Gallery::Application.routes.draw do
  resources :apps

  devise_for :users

  # examples
  resources :example_photos

  mount Sidekiq::Web => '/sidekiq'
  root to: 'home#hello'
end
