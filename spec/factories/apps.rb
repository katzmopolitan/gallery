# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :app do
    name "MyString"
    url "MyString"
    image "MyString"
    user nil
  end
end
