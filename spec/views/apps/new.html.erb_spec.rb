require 'spec_helper'

describe "apps/new" do
  before(:each) do
    assign(:app, stub_model(App,
      :name => "MyString",
      :url => "MyString",
      :image => "MyString",
      :user => nil
    ).as_new_record)
  end

  it "renders new app form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", apps_path, "post" do
      assert_select "input#app_name[name=?]", "app[name]"
      assert_select "input#app_url[name=?]", "app[url]"
      assert_select "input#app_image[name=?]", "app[image]"
      assert_select "input#app_user[name=?]", "app[user]"
    end
  end
end
